#include <iostream>
#include <string>
#include "CPUID.h"
#include "Helpers.h"
#define RAND_GENERATE(X) \
do\
{\
	X;\
}while (randNum > 1024);\

using namespace std;
inline void print(string_view MyText)
{
	cout << MyText << endl;
}
unsigned generateData(bool supportRDRAND)
{
	unsigned randNum = 0;
	if (supportRDRAND)
	{
		RAND_GENERATE(_rdrand32_step(&randNum))
	}
	else
	{
		RAND_GENERATE(randNum = rand())
	}
	
	return randNum;
}
int main()
{
	const unsigned randNum1 = generateData(InstructionSet::RDRAND());
	const unsigned randNum2 = generateData(InstructionSet::RDRAND());
	print("First rand " + to_string(randNum1));
	print("Second rand " + to_string(randNum2));
	const auto tmpData=square<unsigned>(randNum1, randNum2);
	print("Result " + to_string(tmpData));
	return EXIT_SUCCESS;
}
