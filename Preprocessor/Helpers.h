#pragma once
template <typename T, typename std::enable_if<std::is_arithmetic<T>::value>::type * = nullptr>
T square(T arg1,T arg2)
{
	return (arg1 + arg2) * (arg1 + arg2);
}